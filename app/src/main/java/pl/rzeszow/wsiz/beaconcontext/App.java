package pl.rzeszow.wsiz.beaconcontext;

import android.app.Application;

import pl.rzeszow.wsiz.bc_library.BeaconContext;

/**
 * Created by Roman Savka on 02.03.2016.
 */
public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        BeaconContext.initialize(this, Config.BEACON_CONTEXT_APP_ID, Config.BEACON_CONTEXT_API_KEY);
    }
}
