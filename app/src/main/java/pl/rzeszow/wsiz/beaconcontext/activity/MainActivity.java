package pl.rzeszow.wsiz.beaconcontext.activity;

import android.Manifest;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import pl.rzeszow.wsiz.bc_library.helper.AskPermissionHelper;
import pl.rzeszow.wsiz.beaconcontext.R;

public class MainActivity extends AppCompatActivity {

    private AskPermissionHelper permissionHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Go WSIiZ !", Snackbar.LENGTH_LONG)
                        .setAction("Go", null).show();
            }
        });
        permissionHelper = AskPermissionHelper.attach(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        permissionHelper.askPermission(Manifest.permission.ACCESS_FINE_LOCATION,
                new AskPermissionHelper.AskPermissionListener() {
                    @Override
                    public void permissionGranted(String permission) {

                    }
                });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
