package pl.rzeszow.wsiz.bc_library.helper;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.annotation.StringDef;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;

import pl.rzeszow.wsiz.bc_library.api.ApiCallManager;
import pl.rzeszow.wsiz.bc_library.model.BeaconAction;
import pl.rzeszow.wsiz.bc_library.model.BeaconParams;
import pl.rzeszow.wsiz.bc_library.model.Proximity;
import pl.rzeszow.wsiz.bc_library.reciever.ActionReceiver;
import pl.rzeszow.wsiz.bc_library.utils.Logger;
import pl.rzeszow.wsiz.beacon_context_library.R;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Roman Savka on 09.03.2016.
 */
public class ActionHelper {

    public static final String ON_ENTER = "On enter";
    public static final String ON_FAR_RANGE = "On far range";
    public static final String ON_NEAR_RANGE = "On near range";
    public static final String ON_IMMEDIATE_RANGE = "On immediate range";
    public static final String ON_EXIT = "On exit";

    public static void handleAction(Context context, BeaconParams params, Proximity proximity) {
        switch (proximity) {
            case FAR:
                handleAction(context, params, ON_FAR_RANGE);
                break;
            case NEAR:
                handleAction(context, params, ON_NEAR_RANGE);
                break;
            case IMMEDIATE:
                handleAction(context, params, ON_IMMEDIATE_RANGE);
                break;
        }
    }

    public static void handleAction(final Context context, BeaconParams params, @Action String action) {
        params = DataManager.getInstance().matchParams(params);
        if (params != null) {
            ApiCallManager.getEndpoint().getActions(params.getId(), action).enqueue(
                    new Callback<List<BeaconAction>>() {
                        @Override
                        public void onResponse(Response<List<BeaconAction>> response, Retrofit retrofit) {
                            if (response.isSuccess()) {
                                dispatchAction(context, response.body());
                            } else {
                                Logger.d("Action endpoint failed");
                                Logger.d("code:" + response.code());
                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            Logger.d(t.getMessage());
                        }
                    });
        }
    }

    private static void dispatchAction(Context context, List<BeaconAction> actions) {
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        for (BeaconAction a : actions) {
            String title = a.getNotificationTitle();
            String description = a.getNotificationMsg();
            if (title != null && description != null) {
                NotificationCompat.Builder builder =
                        new NotificationCompat.Builder(context)
                                .setSmallIcon(R.drawable.ic_notification)
                                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.logo_notification))
                                .setContentTitle(title)
                                .setContentText(description)
                                .setWhen(System.currentTimeMillis())
                                .setContentIntent(createContentIntent(context, a))
                                .setDeleteIntent(createDeleteIntent(context, a))
                                .setAutoCancel(true)
                                .setStyle(new NotificationCompat.BigTextStyle()
                                        .setBigContentTitle(title)
                                        .bigText(description));
                notificationManager.notify(a.getId(), builder.build());
                context.sendBroadcast(getBroadcastIntent(context, ActionReceiver.SEND_ACTION_NOTIFICATION, a));
            } else {
                context.sendBroadcast(getBroadcastIntent(context, ActionReceiver.RECEIVE_ACTION, a));
            }
        }
    }

    private static PendingIntent createContentIntent(Context context, BeaconAction action) {
        return PendingIntent.getBroadcast(context, 0, getBroadcastIntent(context, ActionReceiver.TAP_ACTION_NOTIFICATION, action),
                PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private static PendingIntent createDeleteIntent(Context context, BeaconAction action) {
        return PendingIntent.getBroadcast(context, 0, getBroadcastIntent(context, ActionReceiver.DISMISS_ACTION_NOTIFICATION, action),
                PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private static Intent getBroadcastIntent(Context context, String action, BeaconAction beaconAction) {
        Intent intent = new Intent(context, ActionReceiver.class);
        intent.setAction(action);
        intent.putExtra(ActionReceiver.ACTION, beaconAction);
        return intent;
    }


    @Retention(RetentionPolicy.CLASS)
    @StringDef({ON_ENTER, ON_EXIT, ON_FAR_RANGE, ON_NEAR_RANGE, ON_IMMEDIATE_RANGE})
    public @interface Action {
    }

}
