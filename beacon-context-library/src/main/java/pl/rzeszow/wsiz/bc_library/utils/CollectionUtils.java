package pl.rzeszow.wsiz.bc_library.utils;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Roman Savka on 09.03.2016.
 */
public class CollectionUtils {

    public static <K, V> void removeMapKeysByList(Map<K, V> map, List<K> list) {
        for (K element : list) {
            for (Iterator<Map.Entry<K, V>> it = map.entrySet().iterator(); it.hasNext(); ) {
                Map.Entry<K, V> entry = it.next();
                if (entry.getKey().equals(element)) {
                    it.remove();
                }
            }
        }
    }
}
