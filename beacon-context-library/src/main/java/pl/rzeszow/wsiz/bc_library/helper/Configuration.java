package pl.rzeszow.wsiz.bc_library.helper;

import android.content.Context;
import android.content.SharedPreferences;

import pl.rzeszow.wsiz.bc_library.utils.StorageManager;

/**
 * Created by Roman Savka on 04.03.2016.
 */
public final class Configuration {

    public static final String APP_ID = "APP_ID";
    public static final String API_KEY = "API_KEY";

    private static String appId;
    private static String apiKey;
    private static boolean hasChanged;

    private Configuration() {
    }

    public static synchronized void initWith(Context context, String appId, String apiKey) {
        if (appId == null || apiKey == null) {
            throw new IllegalArgumentException("Api key or app id is not valid");
        }
        SharedPreferences preferences = StorageManager.getPreferences(context, StorageManager.GENERAL);
        String savedAppId = preferences.getString(APP_ID, null);
        String savedApiKey = preferences.getString(API_KEY, null);
        hasChanged = savedAppId != null && savedApiKey != null &&
                (!savedAppId.equals(appId) || !savedApiKey.equals(apiKey));

        Configuration.appId = appId;
        Configuration.apiKey = apiKey;

        preferences.edit()
                .putString(APP_ID, appId)
                .putString(API_KEY, apiKey)
                .apply();
    }

    public static boolean hasChanged() {
        return hasChanged;
    }

    public static String getAppId() {
        return appId;
    }

    public static String getApiKey() {
        return apiKey;
    }
}
