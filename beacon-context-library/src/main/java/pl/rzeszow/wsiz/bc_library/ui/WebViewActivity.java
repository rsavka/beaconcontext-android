package pl.rzeszow.wsiz.bc_library.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

import pl.rzeszow.wsiz.bc_library.model.ActionAttr;
import pl.rzeszow.wsiz.bc_library.model.BeaconAction;
import pl.rzeszow.wsiz.beacon_context_library.R;

public class WebViewActivity extends AppCompatActivity {

    public static final String ACTION = "ACTION";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        WebView webView = (WebView) findViewById(R.id.webView);
        BeaconAction action = (BeaconAction) getIntent().getSerializableExtra(ACTION);
        webView.loadData(action.getAttr(ActionAttr.HTML_KEY), "text/html", "utf-8");
    }
}
