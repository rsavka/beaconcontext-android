package pl.rzeszow.wsiz.bc_library.utils;

import java.util.ArrayDeque;
import java.util.Queue;

/**
 * Created by Roman Savka on 05.03.2016.
 */
public class RunnableQueue {

    private boolean queueActive = false;
    private Queue<Runnable> actionsQueue;

    public void setQueueActive() {
        queueActive = true;
    }

    public void setQueueInActive() {
        queueActive = false;
    }

    public void executeAction(Runnable action) {
        if (queueActive) {
            action.run();
        } else {
            initializeActionsQueue();
            actionsQueue.add(action);
        }
    }

    private void initializeActionsQueue() {
        if (actionsQueue == null) {
            actionsQueue = new ArrayDeque<>();
        }
    }

    public void clearActionsQueue() {
        if (actionsQueue != null) {
            actionsQueue.clear();
        }
    }

    public void executeActions() {
        if (actionsQueue != null) {
            while (true) {
                Runnable action = actionsQueue.poll();
                if (action == null) {
                    break;
                }
                action.run();
            }
        }
    }
}