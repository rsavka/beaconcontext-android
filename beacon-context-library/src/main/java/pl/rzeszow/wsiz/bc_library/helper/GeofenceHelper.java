package pl.rzeszow.wsiz.bc_library.helper;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;

import java.util.Collections;
import java.util.List;

import pl.rzeszow.wsiz.bc_library.service.GeofenceTransitionService;
import pl.rzeszow.wsiz.bc_library.utils.Logger;
import pl.rzeszow.wsiz.bc_library.utils.RunnableQueue;
import pl.rzeszow.wsiz.bc_library.utils.StorageManager;

/**
 * Created by Roman Savka on 05.03.2016.
 */
public final class GeofenceHelper implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, ResultCallback<Status> {

    private static final String INFERNAL_ERROR = "INFERNAL_ERROR";
    private static GeofenceHelper instance;

    private GoogleApiClient googleApiClient;
    private RunnableQueue runnableQueue;
    private Context context;

    private GeofenceHelper(Context context) {
        if (!isGooglePlayServicesAvailable(context)) {
            return;
        }
        this.context = context;
        this.runnableQueue = new RunnableQueue();
        this.googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        this.runnableQueue.setQueueInActive();
        this.googleApiClient.connect();
    }

    public static GeofenceHelper getInstance(Context context) {
        if (instance == null) {
            instance = new GeofenceHelper(context);
        }
        return instance;
    }

    private boolean isGoogleApiClientAvailable() {
        return googleApiClient != null;
    }

    private boolean isGoogleApiClientConnected() {
        return googleApiClient.isConnected();
    }

    private boolean isGooglePlayServicesAvailable(Context context) {
        int resultCode = GoogleApiAvailability.getInstance()
                .isGooglePlayServicesAvailable(context);
        if (ConnectionResult.SUCCESS == resultCode) {
            return true;
        } else {
            Logger.d("Google Play services is unavailable.");
            return false;
        }
    }

    public void add(Geofence geofence) {
        add(Collections.singletonList(geofence));
    }

    public void add(final List<Geofence> geofences) {
        if (!isGoogleApiClientAvailable()) {
            Logger.d("Google api not available");
            return;
        }
        if (isGoogleApiClientConnected()) {
            addGeofence(geofences);
        } else {
            runnableQueue.executeAction(new Runnable() {
                @Override
                public void run() {
                    addGeofence(geofences);
                }
            });
        }
    }

    public void remove(String geofenceId) {
        remove(Collections.singletonList(geofenceId));
    }

    public void remove(final List<String> geofenceIds) {
        if (!isGoogleApiClientAvailable()) {
            Logger.d("Google api not available");
            return;
        }
        if (isGoogleApiClientConnected()) {
            removeGeofence(geofenceIds);
        } else {
            runnableQueue.executeAction(new Runnable() {
                @Override
                public void run() {
                    removeGeofence(geofenceIds);
                }
            });
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        runnableQueue.setQueueActive();
        runnableQueue.executeActions();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Logger.d("Connection with location services suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        Logger.d("Connection with location services failed");
        runnableQueue.clearActionsQueue();
    }

    private void addGeofence(List<Geofence> geofences) {
        //noinspection ResourceType
        LocationServices.GeofencingApi
                .addGeofences(googleApiClient, getGeofencingRequest(geofences), getGeofenceIntent())
                .setResultCallback(this);
    }

    private void removeGeofence(List<String> geofenceIds) {
        LocationServices.GeofencingApi
                .removeGeofences(googleApiClient, geofenceIds)
                .setResultCallback(this);
    }

    private GeofencingRequest getGeofencingRequest(List<Geofence> geofences) {
        return new GeofencingRequest.Builder()
                .setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER | GeofencingRequest.INITIAL_TRIGGER_DWELL)
                .addGeofences(geofences).build();
    }

    private PendingIntent getGeofenceIntent() {
        Intent intent = new Intent(context, GeofenceTransitionService.class);
        return PendingIntent.getService(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    @Override
    public void onResult(@NonNull Status status) {
        if (status.isSuccess()) {
            Logger.d("Location Services add success");
        } else {
            Logger.d("Location Services add error code: " + status.getStatusCode());
            Logger.d("Location Services error msg: " + status.getStatusMessage());
            putInternalErrorFlag(context);
        }
    }

    private static void putInternalErrorFlag(Context context) {
        StorageManager.getPreferences(context, StorageManager.GENERAL)
                .edit().putBoolean(INFERNAL_ERROR, true).apply();
    }

    public static void removeInternalErrorFlag(Context context) {
        StorageManager.getPreferences(context, StorageManager.GENERAL)
                .edit().remove(INFERNAL_ERROR).apply();
    }

    public static boolean wasInternalError(Context context) {
        return StorageManager.getPreferences(context, StorageManager.GENERAL)
                .getBoolean(INFERNAL_ERROR, false);
    }

}
