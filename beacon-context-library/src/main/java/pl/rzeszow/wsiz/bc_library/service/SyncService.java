package pl.rzeszow.wsiz.bc_library.service;

import android.app.AlarmManager;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import pl.rzeszow.wsiz.bc_library.helper.AlarmHelper;
import pl.rzeszow.wsiz.bc_library.helper.DataManager;
import pl.rzeszow.wsiz.bc_library.reciever.AlarmReceiver;

/**
 * Created by Roman Savka on 04.03.2016.
 */
public class SyncService extends IntentService {

    // test: 5 * 60 * 1000
    private static final long SYNC_INTERVAL = AlarmManager.INTERVAL_DAY;

    public static void schedule(Context context) {
        AlarmHelper.addRepeatingAlarm(context, AlarmReceiver.SYNC_DATA, SYNC_INTERVAL);
    }

    public static void start(Context context) {
        context.startService(new Intent(context, SyncService.class));
    }

    public SyncService() {
        super(SyncService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        DataManager.getInstance().load();
        this.stopSelf();
    }
}