package pl.rzeszow.wsiz.bc_library.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Roman Savka on 03.03.2016.
 */
public class ActionAttr implements Serializable {

    public static final String NOTIFICATION_TITLE_KEY = "notification_title";
    public static final String NOTIFICATION_MSG_KEY = "notification_msg";
    public static final String HTML_KEY = "html";
    public static final String LINK_KEY = "link";

    @SerializedName("key")
    private String key;
    @SerializedName("type")
    private String type;
    @SerializedName("value")
    private String value;

    public String getKey() {
        return key;
    }

    public String getType() {
        return type;
    }

    public String getValue() {
        return value;
    }
}
