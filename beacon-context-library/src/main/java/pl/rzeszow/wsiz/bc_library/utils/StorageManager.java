package pl.rzeszow.wsiz.bc_library.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.StringDef;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Roman Savka on 02.03.2016.
 */
public class StorageManager {

    public static final String GENERAL = "GENERAL";
    public static final String PERMISSIONS = "PERMISSIONS";

    public static SharedPreferences getPreferences(Context context, @PrefScope String scope) {
        return context.getSharedPreferences(scope, Context.MODE_PRIVATE);
    }

    @SuppressWarnings("unchecked")
    public static <T> T readFromFile(Context context, String fileName) {
        try {
            FileInputStream fis = context.openFileInput(fileName);
            ObjectInputStream is = new ObjectInputStream(fis);
            T data = (T) is.readObject();
            is.close();
            return data;
        } catch (Exception ex) {
            Logger.d(ex.getMessage());
            return null;
        }
    }

    public static <T> void saveToFile(Context context, String fileName, T object) {
        try {
            FileOutputStream fos = context.openFileOutput(fileName, Context.MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(object);
            os.close();
        } catch (Exception ex) {
            Logger.d(ex.getMessage());
        }
    }

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({GENERAL, PERMISSIONS})
    public @interface PrefScope {
    }
}
