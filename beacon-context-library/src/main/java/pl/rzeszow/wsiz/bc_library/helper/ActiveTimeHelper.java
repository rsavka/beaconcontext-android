package pl.rzeszow.wsiz.bc_library.helper;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.Nullable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import pl.rzeszow.wsiz.bc_library.model.ActiveTime;
import pl.rzeszow.wsiz.bc_library.reciever.AlarmReceiver;
import pl.rzeszow.wsiz.bc_library.utils.CalendarUtils;
import pl.rzeszow.wsiz.bc_library.utils.Logger;
import pl.rzeszow.wsiz.bc_library.utils.UriFactory;

/**
 * Created by Roman Savka on 05.03.2016.
 */
public final class ActiveTimeHelper {

    private static SimpleDateFormat hoursFormat = new SimpleDateFormat("HH:mm:ss", Locale.UK);

    private ActiveTimeHelper() {
    }

    public static void schedule(Context context, List<ActiveTime> times) {
        if (times == null || times.isEmpty()) {
            return;
        }
        for (ActiveTime time : times) {
            if (time.hasDateAndHours()) {
                handleDateAndHours(context, time);
            } else if (time.hasOnlyDates()) {
                handleOnlyDates(context, time);
            } else if (time.hasOnlyHours()) {
                handleOnlyHours(context, time);
            }
        }
    }

    private static void handleDateAndHours(Context context, ActiveTime time) {
        Calendar startHour = parseHour(time.getStartHour());
        Calendar endHour = parseHour(time.getEndHour());
        if (startHour == null || endHour == null) {
            return;
        }
        Uri alarmUri = getAlarmUri(time);
        Calendar alarm = AlarmHelper.getCalendar(time.getStartDate());
        CalendarUtils.copy(alarm, startHour, Calendar.HOUR_OF_DAY, Calendar.MINUTE);
        AlarmHelper.addOnceForDayAlarm(context, AlarmReceiver.START_LOCATION_MONITORING, alarmUri, alarm);
        CalendarUtils.copy(alarm, endHour, Calendar.HOUR_OF_DAY, Calendar.MINUTE);
        AlarmHelper.addOnceForDayAlarm(context, AlarmReceiver.STOP_LOCATION_MONITORING, alarmUri, alarm);
    }

    private static void handleOnlyDates(Context context, ActiveTime time) {
        Uri alarmUri = getAlarmUri(time);
        Calendar alarm = AlarmHelper.getCalendar(time.getStartDate());
        AlarmHelper.addOneShotAlarm(context, AlarmReceiver.START_LOCATION_MONITORING, alarmUri, alarm);
        alarm = AlarmHelper.getCalendar(time.getEndDate());
        AlarmHelper.addOneShotAlarm(context, AlarmReceiver.STOP_LOCATION_MONITORING, alarmUri, alarm);
    }

    private static void handleOnlyHours(Context context, ActiveTime time) {
        Calendar startHour = parseHour(time.getStartHour());
        Calendar endHour = parseHour(time.getEndHour());
        if (startHour == null || endHour == null) {
            return;
        }
        Uri alarmUri = getAlarmUri(time);
        Calendar alarm = AlarmHelper.getNowCalendar();
        CalendarUtils.copy(alarm, startHour, Calendar.HOUR_OF_DAY, Calendar.MINUTE);
        AlarmHelper.addOnceForDayAlarm(context, AlarmReceiver.START_LOCATION_MONITORING, alarmUri, alarm);
        CalendarUtils.copy(alarm, endHour, Calendar.HOUR_OF_DAY, Calendar.MINUTE);
        AlarmHelper.addOnceForDayAlarm(context, AlarmReceiver.STOP_LOCATION_MONITORING, alarmUri, alarm);
    }

    public static void remove(Context context, List<ActiveTime> times) {
        if (times == null || times.isEmpty()) {
            return;
        }
        for (ActiveTime time : times) {
            AlarmHelper.removeAlarm(context, AlarmReceiver.START_LOCATION_MONITORING, getAlarmUri(time));
            AlarmHelper.removeAlarm(context, AlarmReceiver.STOP_LOCATION_MONITORING, getAlarmUri(time));
        }
    }

    @Nullable
    private static Calendar parseHour(String hour) {
        try {
            Date date = hoursFormat.parse(hour);
            return AlarmHelper.getCalendar(date);
        } catch (ParseException e) {
            Logger.d(e.getMessage());
            return null;
        }
    }

    private static Uri getAlarmUri(ActiveTime time) {
        return UriFactory.createAlarmUri(time.getGeoAreaId(), time.getId());
    }
}