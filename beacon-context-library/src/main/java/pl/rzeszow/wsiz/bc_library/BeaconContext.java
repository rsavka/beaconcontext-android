package pl.rzeszow.wsiz.bc_library;

import android.Manifest;
import android.app.Application;

import com.kontakt.sdk.android.common.KontaktSDK;

import pl.rzeszow.wsiz.bc_library.api.ApiCallManager;
import pl.rzeszow.wsiz.bc_library.helper.AskPermissionHelper;
import pl.rzeszow.wsiz.bc_library.helper.Configuration;
import pl.rzeszow.wsiz.bc_library.helper.DataManager;
import pl.rzeszow.wsiz.bc_library.service.SyncService;
import pl.rzeszow.wsiz.bc_library.utils.Logger;

/**
 * Created by Roman Savka on 02.03.2016.
 */
public final class BeaconContext {

    private BeaconContext() {
    }

    public static void initialize(Application application, String appId, String apiKey) {
        Logger.d("initialize");
        if (!AskPermissionHelper.isPermissionGranted(application, Manifest.permission.ACCESS_FINE_LOCATION)) {
            Logger.d("Location permission is not granted.");
            return;
        }
        KontaktSDK.initialize(Config.KONTAKT_IO_API_KEY);
        Configuration.initWith(application, appId, apiKey);
        DataManager.initialize(application);
        if (Configuration.hasChanged()) {
            DataManager.getInstance().clear();
        }
        ApiCallManager.initServiceEndpoint(application);
        DataManager.getInstance().load();
        SyncService.schedule(application);
    }

}
