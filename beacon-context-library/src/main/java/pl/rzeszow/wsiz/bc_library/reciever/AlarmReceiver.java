package pl.rzeszow.wsiz.bc_library.reciever;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.google.android.gms.location.Geofence;

import pl.rzeszow.wsiz.bc_library.helper.DataManager;
import pl.rzeszow.wsiz.bc_library.helper.GeofenceHelper;
import pl.rzeszow.wsiz.bc_library.model.GeoArea;
import pl.rzeszow.wsiz.bc_library.service.SyncService;
import pl.rzeszow.wsiz.bc_library.utils.GeofenceFactory;
import pl.rzeszow.wsiz.bc_library.utils.Logger;
import pl.rzeszow.wsiz.bc_library.utils.UriFactory;

/**
 * Created by Roman Savka on 04.03.2016.
 */
public class AlarmReceiver extends BroadcastReceiver {

    private static final String TAG = "AlarmReceiver";

    public static final String START_LOCATION_MONITORING =
            "pl.rzeszow.wsiz.bc_library.receiver.START_LOCATION_MONITORING";
    public static final String STOP_LOCATION_MONITORING =
            "pl.rzeszow.wsiz.bc_library.receiver.STOP_LOCATION_MONITORING";
    public static final String SYNC_DATA =
            "pl.rzeszow.wsiz.bc_library.receiver.SYNC_DATA";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (START_LOCATION_MONITORING.equals(intent.getAction())) {
            GeoArea area = DataManager.getInstance()
                    .getArea(intent.getData().getQueryParameter(UriFactory.AREA_ID));
            Geofence geofence = GeofenceFactory.createGeofence(area);
            GeofenceHelper.getInstance(context).add(geofence);
        } else if (STOP_LOCATION_MONITORING.equals(intent.getAction())) {
            String areaId = intent.getData().getQueryParameter(UriFactory.AREA_ID);
            GeofenceHelper.getInstance(context).remove(areaId);
        } else if (SYNC_DATA.equals(intent.getAction())) {
            SyncService.start(context);
        }
        String[] split = intent.getAction().split("\\.");
        Logger.d("Receive alarm, action:" + split[split.length - 1]);
        if (intent.getData() != null) {
            Logger.d("Area id: " + intent.getData().getQueryParameter(UriFactory.AREA_ID) +
                    " Time id: " + intent.getData().getQueryParameter(UriFactory.TIME_ID));
        }
    }
}
