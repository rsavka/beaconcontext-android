package pl.rzeszow.wsiz.bc_library.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Roman Savka on 03.03.2016.
 */
public class ActiveTime implements Serializable {

    @SerializedName("id")
    private Integer id;
    @SerializedName("startDate")
    private Date startDate;
    @SerializedName("endDate")
    private Date endDate;
    @SerializedName("startHour")
    private String startHour;
    @SerializedName("endHour")
    private String endHour;
    @SerializedName("createdAt")
    private Date createdAt;
    @SerializedName("updatedAt")
    private Date updatedAt;
    @SerializedName("geoAreaId")
    private Integer geoAreaId;

    public Integer getId() {
        return id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public String getStartHour() {
        return startHour;
    }

    public String getEndHour() {
        return endHour;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public Integer getGeoAreaId() {
        return geoAreaId;
    }

    public boolean hasDateAndHours() {
        return startDate != null && endDate != null && startHour != null && endHour != null;
    }

    public boolean hasOnlyDates() {
        return startDate != null && endDate != null && startHour == null && endHour == null;
    }

    public boolean hasOnlyHours() {
        return startDate == null && endDate == null && startHour != null && endHour != null;
    }
}
