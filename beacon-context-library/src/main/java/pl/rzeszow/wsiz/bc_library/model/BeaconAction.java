package pl.rzeszow.wsiz.bc_library.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by Roman Savka on 02.03.2016.
 */
public class BeaconAction implements Serializable {

    @SerializedName("id")
    private Integer id;
    @SerializedName("name")
    private String name;
    @SerializedName("type")
    private String type;
    @SerializedName("createdAt")
    private Date createdAt;
    @SerializedName("updatedAt")
    private Date updatedAt;
    @SerializedName("beaconId")
    private Integer beaconId;

    @SerializedName("attributes")
    private List<ActionAttr> attributes;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public Integer getBeaconId() {
        return beaconId;
    }

    public List<ActionAttr> getAttributes() {
        return attributes;
    }

    public String getNotificationTitle() {
        return getAttr(ActionAttr.NOTIFICATION_TITLE_KEY);
    }

    public String getNotificationMsg() {
        return getAttr(ActionAttr.NOTIFICATION_MSG_KEY);
    }

    public String getAttr(String key) {
        for (ActionAttr a : attributes) {
            if (a.getKey().equals(key)) {
                return a.getValue();
            }
        }
        return null;
    }
}
