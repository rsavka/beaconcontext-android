package pl.rzeszow.wsiz.bc_library.utils;

import android.util.Log;

/**
 * Created by Roman Savka on 05.03.2016.
 */
public final class Logger {

    private static final String TAG = "BeaconContext:Logger";

    private Logger() {
    }

    public static void d(Throwable t) {
        if (t != null) {
            Log.d(TAG, Log.getStackTraceString(t));
        }
    }

    public static void d(String msg) {
        if (msg != null) {
            Log.d(TAG, msg);
        }
    }

}
