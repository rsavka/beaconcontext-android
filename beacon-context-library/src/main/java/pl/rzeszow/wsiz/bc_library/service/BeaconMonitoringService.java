package pl.rzeszow.wsiz.bc_library.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.annotation.StringDef;

import com.kontakt.sdk.android.ble.configuration.ActivityCheckConfiguration;
import com.kontakt.sdk.android.ble.configuration.ForceScanConfiguration;
import com.kontakt.sdk.android.ble.configuration.ScanPeriod;
import com.kontakt.sdk.android.ble.configuration.scan.IBeaconScanContext;
import com.kontakt.sdk.android.ble.configuration.scan.ScanContext;
import com.kontakt.sdk.android.ble.connection.OnServiceReadyListener;
import com.kontakt.sdk.android.ble.device.BeaconDevice;
import com.kontakt.sdk.android.ble.device.BeaconRegion;
import com.kontakt.sdk.android.ble.discovery.BluetoothDeviceEvent;
import com.kontakt.sdk.android.ble.discovery.ibeacon.IBeaconAdvertisingPacket;
import com.kontakt.sdk.android.ble.filter.ibeacon.IBeaconFilter;
import com.kontakt.sdk.android.ble.manager.ProximityManager;
import com.kontakt.sdk.android.ble.manager.ProximityManagerContract;
import com.kontakt.sdk.android.common.profile.DeviceProfile;
import com.kontakt.sdk.android.common.profile.RemoteBluetoothDevice;

import java.io.Serializable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import pl.rzeszow.wsiz.bc_library.helper.ActionHelper;
import pl.rzeszow.wsiz.bc_library.model.BeaconParams;
import pl.rzeszow.wsiz.bc_library.model.Proximity;
import pl.rzeszow.wsiz.bc_library.utils.CollectionUtils;
import pl.rzeszow.wsiz.bc_library.utils.Logger;
import pl.rzeszow.wsiz.bc_library.utils.StorageManager;


/**
 * Created by Roman Savka on 06.03.2016.
 */
public class BeaconMonitoringService extends Service implements ProximityManager.ProximityListener {

    private static final String TAG = "BeaconMonitoringService";

    public static final String ADD_BEACONS = "pl.rzeszow.wsiz.bc_library.service.ADD_BEACONS";
    public static final String REMOVE_BEACONS = "pl.rzeszow.wsiz.bc_library.service.REMOVE_BEACONS";
    public static final String BEACONS = "BEACONS";
    private static final String BEACONS_FILE_NAME = "BEACONS_FILE_NAME";

    private ProximityManagerContract proximityManager;
    private Data monitoringData;
    private ScanContext scanContext;

    public static void command(Context context, @Action String action, List<BeaconParams> beacons) {
        Intent intent = new Intent(context, BeaconMonitoringService.class);
        intent.setAction(action);
        intent.putExtra(BEACONS, new Data(beacons));
        context.startService(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Logger.d("Beacon monitoring onCreate " + this.hashCode());
        proximityManager = new ProximityManager(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            if (ADD_BEACONS.equals(intent.getAction())) {
                addBeacons(((Data) intent.getSerializableExtra(BEACONS)).beacons);
            } else if (REMOVE_BEACONS.equals(intent.getAction())) {
                removeBeacons(((Data) intent.getSerializableExtra(BEACONS)).beacons);
            }
        } else if (monitoringData == null) {
            Logger.d("Beacon monitoring read file");
            monitoringData = StorageManager.readFromFile(this, BEACONS_FILE_NAME);
        }
        if (monitoringData == null || monitoringData.isEmpty()) {
            Logger.d("Beacon monitoring stop self " + this.hashCode());
            proximityManager.detachListener(this);
            proximityManager.disconnect();
            stopSelf();
            return START_NOT_STICKY;
        }
        initManager();
        Logger.d("Beacon monitoring onStartCommand " + this.hashCode());
        return Service.START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Logger.d("Beacon monitoring onDestroy " + this.hashCode());
    }

    private void initManager() {
        proximityManager.initializeScan(getScanContext(), new OnServiceReadyListener() {
            @Override
            public void onServiceReady() {
                proximityManager.attachListener(BeaconMonitoringService.this);
            }

            @Override
            public void onConnectionFailure() {
                Logger.d("Proximity manager connection failed");
            }
        });
    }

    private ScanContext getScanContext() {
        if (scanContext == null) {
            scanContext = new ScanContext.Builder()
                    .setScanPeriod(ScanPeriod.RANGING)
                    .setScanMode(ProximityManager.SCAN_MODE_BALANCED)
                    .setActivityCheckConfiguration(ActivityCheckConfiguration.MINIMAL)
                    .setForceScanConfiguration(ForceScanConfiguration.MINIMAL)
                    .setIBeaconScanContext(getBeaconScanContext())
                    .build();
        }
        return scanContext;
    }

    private IBeaconScanContext getBeaconScanContext() {
        return new IBeaconScanContext.Builder()
                .setDevicesUpdateCallbackInterval(TimeUnit.SECONDS.toMillis(1))
                .setIBeaconFilters(Collections.singletonList(new IBeaconFilter() {
                    @Override
                    public boolean apply(IBeaconAdvertisingPacket packet) {
                        for (BeaconParams param : monitoringData.beacons) {
                            if (packet.getProximityUUID().toString().equals(param.getUuid()) &&
                                    (param.getMajor() == BeaconRegion.ANY_MAJOR || param.getMajor() == packet.getMajor()) &&
                                    (param.getMinor() == BeaconRegion.ANY_MINOR || param.getMinor() == packet.getMinor())) {
                                return true;
                            }
                        }
                        return false;
                    }
                }))
                .build();
    }

    private void addBeacons(List<BeaconParams> beacons) {
        if (monitoringData == null) {
            monitoringData = new Data();
        }
        monitoringData.beacons.addAll(beacons);
        StorageManager.saveToFile(this, BEACONS_FILE_NAME, monitoringData);
    }

    private void removeBeacons(List<BeaconParams> beacons) {
        if (monitoringData == null) {
            return;
        }
        if (monitoringData.lastProximityMap != null) {
            CollectionUtils.removeMapKeysByList(monitoringData.lastProximityMap, beacons);
        }
        monitoringData.beacons.removeAll(beacons);
        StorageManager.saveToFile(this, BEACONS_FILE_NAME, monitoringData);
    }

    @Override
    public void onScanStart() {
        Logger.d("scan started");
    }

    @Override
    public void onScanStop() {
        Logger.d("scan stopped");
    }

    @Override
    public void onEvent(BluetoothDeviceEvent event) {
        if (DeviceProfile.IBEACON == event.getDeviceProfile()) {
            Logger.d("BluetoothDeviceEvent timestamp:" + event.getTimestamp());
            List<? extends RemoteBluetoothDevice> devices = event.getDeviceList();
            for (RemoteBluetoothDevice d : devices) {
                Proximity proximity = Proximity.eval(d.getProximity());
                switch (event.getEventType()) {
                    case DEVICE_DISCOVERED:
                        Logger.d("device discovered type, proximity:" + proximity.name());
                        if (Proximity.UNKNOWN == proximity) {
                            ActionHelper.handleAction(this, new BeaconParams((BeaconDevice) d), ActionHelper.ON_ENTER);
                        }
                        break;
                    case DEVICES_UPDATE:
                        BeaconParams params = new BeaconParams((BeaconDevice) d);
                        Proximity lastProximity = monitoringData.lastProximityMap.get(params);
                        if (lastProximity == null || lastProximity != proximity) {
                            Logger.d("proximity changed, proximity:" + proximity.name());
                            monitoringData.lastProximityMap.put(params, proximity);
                            ActionHelper.handleAction(this, params, proximity);
                        }
                        break;
                    case DEVICE_LOST:
                        Logger.d("device lost type, proximity:" + proximity.name());
                        if (Proximity.UNKNOWN == proximity) {
                            ActionHelper.handleAction(this, new BeaconParams((BeaconDevice) d), ActionHelper.ON_EXIT);
                        }
                        break;
                }
            }
        } else {
            Logger.d("not supported protocol");
        }
    }

    @Retention(RetentionPolicy.CLASS)
    @StringDef({ADD_BEACONS, REMOVE_BEACONS})
    public @interface Action {
    }

    private static class Data implements Serializable {
        private List<BeaconParams> beacons;
        private HashMap<BeaconParams, Proximity> lastProximityMap;

        Data() {
            beacons = new ArrayList<>();
            lastProximityMap = new HashMap<>();
        }

        Data(List<BeaconParams> beacons) {
            this.beacons = beacons;
        }

        boolean isEmpty() {
            return beacons.isEmpty();
        }
    }
}
