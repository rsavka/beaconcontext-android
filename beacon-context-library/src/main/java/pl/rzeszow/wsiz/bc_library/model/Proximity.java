package pl.rzeszow.wsiz.bc_library.model;

import java.io.Serializable;

/**
 * Created by Roman Savka on 11.03.2016.
 */
public enum Proximity implements Serializable {

    IMMEDIATE,
    NEAR,
    FAR,
    UNKNOWN;

    public static Proximity eval(com.kontakt.sdk.android.common.Proximity proximity) {
        switch (proximity) {
            case FAR:
                return FAR;
            case NEAR:
                return NEAR;
            case IMMEDIATE:
                return IMMEDIATE;
            case UNKNOWN:
            default:
                return UNKNOWN;
        }
    }

}
