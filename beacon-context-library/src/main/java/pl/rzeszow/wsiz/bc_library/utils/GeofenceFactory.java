package pl.rzeszow.wsiz.bc_library.utils;

import com.google.android.gms.location.Geofence;

import pl.rzeszow.wsiz.bc_library.model.GeoArea;

/**
 * Created by Roman Savka on 05.03.2016.
 */
public class GeofenceFactory {

    public static Geofence createGeofence(GeoArea area) {
        return new Geofence.Builder()
                .setRequestId(area.getId())
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                .setCircularRegion(area.getLatitude(), area.getLongitude(), area.getRadius())
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .build();
    }

}
