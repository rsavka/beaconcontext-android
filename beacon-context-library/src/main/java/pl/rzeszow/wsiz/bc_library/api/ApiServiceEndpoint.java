package pl.rzeszow.wsiz.bc_library.api;

import java.util.List;

import pl.rzeszow.wsiz.bc_library.model.BeaconAction;
import pl.rzeszow.wsiz.bc_library.model.BeaconParams;
import pl.rzeszow.wsiz.bc_library.model.GeoArea;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by Roman Savka on 02.03.2016.
 */
public interface ApiServiceEndpoint {

    @GET("getAreas")
    Call<List<GeoArea>> getAreas();

    @GET("getArea/{areaId}")
    Call<GeoArea> getArea(@Path("areaId") int areaId);

    @GET("getBeacons/{areaId}")
    Call<List<BeaconParams>> getBeacons(@Path("areaId") int areaId);

    @GET("getActions/{beaconId}")
    Call<List<BeaconAction>> getActions(@Path("beaconId") int beaconId, @Query("type") String type);

}
