package pl.rzeszow.wsiz.bc_library.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kontakt.sdk.android.ble.device.BeaconDevice;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by Roman Savka on 02.03.2016.
 */
public class BeaconParams implements Serializable {

    @SerializedName("id")
    private Integer id;
    @SerializedName("tag")
    private String tag;
    @SerializedName("uuid")
    private String uuid;
    @SerializedName("major")
    private Integer major;
    @SerializedName("minor")
    private Integer minor;
    @SerializedName("vendor")
    private String vendor;
    @SerializedName("createdAt")
    private Date createdAt;
    @SerializedName("updatedAt")
    private Date updatedAt;
    @SerializedName("geoAreaId")
    private Integer geoAreaId;

    @SerializedName("actions")
    @Expose(serialize = false, deserialize = true)
    private List<BeaconAction> actions;

    public BeaconParams() {
    }

    public BeaconParams(BeaconDevice device) {
        this.uuid = device.getProximityUUID().toString();
        this.major = device.getMajor();
        this.minor = device.getMinor();
    }

    public Integer getId() {
        return id;
    }

    public String getTag() {
        return tag;
    }

    public String getUuid() {
        return uuid;
    }

    public Integer getMajor() {
        return major == null ? -1 : major;
    }

    public Integer getMinor() {
        return minor == null ? -1 : minor;
    }

    public String getVendor() {
        return vendor;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public Integer getGeoAreaId() {
        return geoAreaId;
    }

    public List<BeaconAction> getActions() {
        return actions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (o instanceof BeaconDevice) {
            o = new BeaconParams((BeaconDevice) o);
        }
        if (!(o instanceof BeaconParams)) {
            return false;
        }
        BeaconParams params = ((BeaconParams) o);
        if (params.getId() != null && this.getId() != null) {
            return params.getId().equals(this.getId());
        } else {
            if (this.uuid != null &&
                    params.uuid != null &&
                    !uuid.equals(params.uuid)) {
                return false;
            }
            if (!this.minor.equals(params.minor) || !this.major.equals(params.major)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((uuid == null) ? 0 : uuid.hashCode());
        result = prime * result
                + ((major == null) ? 0 : major);
        result = prime * result
                + ((major == null) ? 0 : major);
        return result;
    }
}
