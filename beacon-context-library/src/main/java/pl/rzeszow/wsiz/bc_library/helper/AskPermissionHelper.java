package pl.rzeszow.wsiz.bc_library.helper;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import pl.rzeszow.wsiz.bc_library.utils.RunnableQueue;
import pl.rzeszow.wsiz.bc_library.utils.StorageManager;

/**
 * Created by Roman Savka on 05.03.2016.
 */
public class AskPermissionHelper extends Fragment {

    private static final int PERMISSION_REQUEST_CODE = 234;
    private static final String TAG = "AskPermissionHelper";

    private Fragment sourceFragment;
    private AskPermissionListener askPermissionListener;

    private RunnableQueue actionsQueue = new RunnableQueue();

    /**
     * When attach helper to fragment it is important to forward result to this helper
     * issue: https://code.google.com/p/android/issues/detail?id=189121
     *
     * @see #onRequestPermissionsResult(int, String[], int[])
     */
    public static <T extends Fragment> AskPermissionHelper attach(T fragment) {
        return attach(fragment.getChildFragmentManager());
    }

    public static <T extends FragmentActivity> AskPermissionHelper attach(T activity) {
        return attach(activity.getSupportFragmentManager());
    }

    private static AskPermissionHelper attach(FragmentManager manager) {
        AskPermissionHelper instance = (AskPermissionHelper) manager.findFragmentByTag(TAG);
        if (instance == null) {
            instance = new AskPermissionHelper();
            manager.beginTransaction().add(instance, TAG).commit();
        }
        return instance;
    }

    public static boolean isPermissionGranted(Context context, String permission) {
        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }

    private static boolean isNeverAsk(Context context, String permission) {
        return StorageManager.getPreferences(context, StorageManager.PERMISSIONS).getBoolean(permission, false);
    }

    private boolean isPermissionGranted(String permission) {
        return isPermissionGranted(getActivity(), permission);
    }

    private boolean isNeverAsk(String permission) {
        return isNeverAsk(getActivity(), permission);
    }

    private void validateNeverAsk(String permission) {
        if (isNeverAsk(permission)) {
            setNeverAsk(permission, false);
        }
    }

    private void setNeverAsk(String permission, boolean neverAsk) {
        StorageManager.getPreferences(getActivity(), StorageManager.PERMISSIONS).edit()
                .putBoolean(permission, neverAsk)
                .apply();
    }

    public void askPermission(final String permission, AskPermissionListener listener) {
        askPermission(Collections.singletonList(permission), listener);
    }

    public void askPermission(List<String> permissions, AskPermissionListener listener) {
        askPermission(permissions, 0, 0, listener, null);
    }

    /**
     * For each requested permission return one of callbacks to listener. Better to use only when permission are from
     * different groups.
     *
     * @see AskPermissionListener
     */
    public void askPermission(List<String> permissions, int explanationTitle, int explanationMsg,
                              AskPermissionListener listener, DialogInterface.OnCancelListener explanationCancelListener) {
        permissions = new ArrayList<>(permissions);
        Iterator<String> iterator = permissions.iterator();
        while (iterator.hasNext()) {
            String permission = iterator.next();
            if (isPermissionGranted(permission)) {
                validateNeverAsk(permission);
                listener.permissionGranted(permission);
                iterator.remove();
            } else if (isNeverAsk(permission)) {
                listener.neverAsk(permission);
                iterator.remove();
            }
        }
        if (!permissions.isEmpty()) {
            if (explanationTitle != 0 && explanationMsg != 0 && explanationCancelListener != null) {
                showExplanationDialog(explanationTitle, explanationMsg, permissions, listener,
                        explanationCancelListener);
            } else {
                startPermissionRequest(permissions, listener);
            }
        } else {
            listener.askingFinished();
        }
    }

    private void showExplanationDialog(int explanationTitle, int explanationMsg, final List<String> permissions,
                                       final AskPermissionListener listener, DialogInterface.OnCancelListener explanationCancelListener) {
        new AlertDialog.Builder(getActivity()).setTitle(explanationTitle)
                .setMessage(explanationMsg)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startPermissionRequest(permissions, listener);
                    }
                })
                .setOnCancelListener(explanationCancelListener)
                .show();
    }

    private void startPermissionRequest(List<String> permissions, AskPermissionListener listener) {
        askPermissionListener = listener;
        String[] permissionsArray = new String[permissions.size()];
        permissions.toArray(permissionsArray);
        if (sourceFragment != null) {
            sourceFragment.requestPermissions(permissionsArray, PERMISSION_REQUEST_CODE);
        } else {
            requestPermissions(permissionsArray, PERMISSION_REQUEST_CODE);
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getParentFragment() != null) {
            sourceFragment = getParentFragment();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        actionsQueue.setQueueActive();
        actionsQueue.executeActions();
    }

    @Override
    public void onPause() {
        actionsQueue.setQueueInActive();
        actionsQueue.clearActionsQueue();
        super.onPause();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull final String[] permissions,
                                           @NonNull final int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                actionsQueue.executeAction(new Runnable() {
                    @Override
                    public void run() {
                        if (askPermissionListener != null) {
                            deliveryResults(permissions, grantResults);
                        }
                    }
                });
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void deliveryResults(@NonNull final String[] permissions,
                                 @NonNull final int[] grantResults) {
        for (int i = 0; i < permissions.length; i++) {
            String permission = permissions[i];
            if (PackageManager.PERMISSION_GRANTED == grantResults[i]) {
                askPermissionListener.permissionGranted(permissions[i]);
            } else {
                boolean shouldNeverAskAgain = !shouldShowRequestPermissionRationale(permission);
                if (shouldNeverAskAgain) {
                    setNeverAsk(permission, true);
                }
                askPermissionListener.permissionDenied(permission, shouldNeverAskAgain);
            }
        }
        askPermissionListener.askingFinished();
    }

    public abstract static class AskPermissionListener {
        public void neverAsk(String permission) {

        }

        public void permissionGranted(String permission) {

        }

        public void permissionDenied(String permission, boolean shouldNeverAskAgain) {

        }

        public void askingFinished() {

        }
    }
}

