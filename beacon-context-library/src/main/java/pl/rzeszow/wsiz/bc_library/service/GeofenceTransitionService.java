package pl.rzeszow.wsiz.bc_library.service;

import android.app.IntentService;
import android.content.Intent;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import java.util.List;

import pl.rzeszow.wsiz.bc_library.helper.DataManager;
import pl.rzeszow.wsiz.bc_library.model.BeaconParams;
import pl.rzeszow.wsiz.bc_library.utils.Logger;

/**
 * Created by Roman Savka on 05.03.2016.
 */
public class GeofenceTransitionService extends IntentService {

    public GeofenceTransitionService() {
        super(GeofenceTransitionService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        GeofencingEvent geoFenceEvent = GeofencingEvent.fromIntent(intent);
        if (geoFenceEvent.hasError()) {
            int code = geoFenceEvent.getErrorCode();
            Logger.d("Location Services error code: " + code);
            return;
        }
        int transitionType = geoFenceEvent.getGeofenceTransition();
        if (Geofence.GEOFENCE_TRANSITION_ENTER == transitionType) {
            List<Geofence> geofences = geoFenceEvent.getTriggeringGeofences();
            for (Geofence geofence : geofences) {
                Logger.d("Enter: " + geofence.getRequestId());
                List<BeaconParams> beacons = DataManager.getInstance()
                        .getArea(geofence.getRequestId()).getBeacons();
                if (beacons != null && !beacons.isEmpty()) {
                    BeaconMonitoringService
                            .command(getApplicationContext(), BeaconMonitoringService.ADD_BEACONS, beacons);
                }
            }
        } else if (Geofence.GEOFENCE_TRANSITION_EXIT == transitionType) {
            List<Geofence> geofences = geoFenceEvent.getTriggeringGeofences();
            for (Geofence geofence : geofences) {
                Logger.d("Exit: " + geofence.getRequestId());
                List<BeaconParams> beacons = DataManager.getInstance()
                        .getArea(geofence.getRequestId()).getBeacons();
                if (beacons != null && !beacons.isEmpty()) {
                    BeaconMonitoringService
                            .command(this, BeaconMonitoringService.REMOVE_BEACONS, beacons);
                }
            }
        } else {
            Logger.d("Unrecognized geofence transition");
        }
    }
}
