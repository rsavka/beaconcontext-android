package pl.rzeszow.wsiz.bc_library.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;

import java.io.UnsupportedEncodingException;
import java.util.UUID;

/**
 * Created by Roman Savka on 02.03.2016.
 */
public final class DeviceIdFactory {

    private static final String PREFS_DEVICE_ID = "device_id";
    private static UUID uuid;

    private DeviceIdFactory() {
    }

    public static String getUuid(Context context) {
        if (uuid == null) {
            synchronized (DeviceIdFactory.class) {
                SharedPreferences preferences = StorageManager.getPreferences(context, StorageManager.GENERAL);
                final String id = preferences.getString(PREFS_DEVICE_ID, null);
                if (id != null) {
                    // Use the ids previously computed and stored in the prefs file
                    uuid = UUID.fromString(id);
                } else {
                    final String androidId = Settings.Secure
                            .getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
                    // Use the Android ID unless it's broken, in which case fallback on deviceId,
                    // unless it's not available, then fallback on a random number which we store
                    // to a prefs file
                    try {
                        uuid = UUID.nameUUIDFromBytes(androidId.getBytes("utf8"));
                    } catch (UnsupportedEncodingException e) {
                        throw new RuntimeException(e);
                    }
                    // Write the value out to the prefs file
                    preferences.edit().putString(PREFS_DEVICE_ID, uuid.toString()).apply();
                }

            }
        }
        return uuid.toString();
    }
}