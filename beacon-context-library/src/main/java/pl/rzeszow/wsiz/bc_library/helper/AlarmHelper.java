package pl.rzeszow.wsiz.bc_library.helper;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import java.util.Calendar;
import java.util.Date;

import pl.rzeszow.wsiz.bc_library.reciever.AlarmReceiver;

/**
 * Created by Roman Savka on 04.03.2016.
 */
public final class AlarmHelper {

    private AlarmHelper() {
    }

    public static Calendar getNowCalendar() {
        Calendar calendar = getCalendar();
        calendar.setTimeInMillis(System.currentTimeMillis());
        return calendar;
    }

    public static Calendar getCalendar(Date data) {
        Calendar calendar = getCalendar();
        calendar.setTime(data);
        return calendar;
    }

    private static Calendar getCalendar() {
        return Calendar.getInstance();
    }

    public static void addRepeatingAlarm(Context context, String action, long interval) {
        getAlarmManager(context).setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                interval, interval, getAlarmIntent(context, action, null));
    }

    public static void addOnceForDayAlarm(Context context, String action, Uri alarmUri, Calendar triggerData) {
        getAlarmManager(context).setInexactRepeating(AlarmManager.RTC_WAKEUP, triggerData.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, getAlarmIntent(context, action, alarmUri));
    }

    public static void addOneShotAlarm(Context context, String action, Uri alarmUri, Calendar triggerData) {
        getAlarmManager(context).set(AlarmManager.RTC_WAKEUP, triggerData.getTimeInMillis(),
                getAlarmIntent(context, action, alarmUri));
    }

    public static void removeAlarm(Context context, String action) {
        getAlarmManager(context).cancel(getAlarmIntent(context, action, null));
    }

    public static void removeAlarm(Context context, String action, Uri alarmUri) {
        getAlarmManager(context).cancel(getAlarmIntent(context, action, alarmUri));
    }

    private static AlarmManager getAlarmManager(Context context) {
        return (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    }

    private static PendingIntent getAlarmIntent(Context context, String action, Uri alarmUri) {
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.setAction(action);
        if (alarmUri != null) {
            intent.setData(alarmUri);
        }
        return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

}
