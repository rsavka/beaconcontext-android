package pl.rzeszow.wsiz.bc_library.utils;

import android.net.Uri;

/**
 * Created by Roman Savka on 05.03.2016.
 */
public final class UriFactory {

    private static final String SCHEME = "bc";
    private static final String HOST = "beaconcontext";
    private static final String GEOFENCE_PATH = "geofence";
    public static final String AREA_ID = "areaId";
    public static final String TIME_ID = "timeId";

    private UriFactory() {
    }

    public static Uri createAlarmUri(int areaId, int timeId) {
        return new Uri.Builder()
                .scheme(SCHEME)
                .authority(HOST)
                .appendQueryParameter(AREA_ID, String.valueOf(areaId))
                .appendQueryParameter(TIME_ID, String.valueOf(timeId))
                .build();
    }

    public static Uri createGeofenceUri(int areaId) {
        return new Uri.Builder()
                .scheme(SCHEME)
                .authority(HOST)
                .appendPath(GEOFENCE_PATH)
                .appendQueryParameter(AREA_ID, String.valueOf(areaId))
                .build();
    }

}
