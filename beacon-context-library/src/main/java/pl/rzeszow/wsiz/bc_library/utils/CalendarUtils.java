package pl.rzeszow.wsiz.bc_library.utils;

import java.util.Calendar;

/**
 * Created by Roman Savka on 05.03.2016.
 */
public class CalendarUtils {

    public static void copy(Calendar destination, Calendar source, Integer... fields) {
        if (fields == null || fields.length == 0) {
            return;
        }
        for (Integer field : fields) {
            destination.set(field, source.get(field));
        }
    }

}
