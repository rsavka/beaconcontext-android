package pl.rzeszow.wsiz.bc_library.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by Roman Savka on 02.03.2016.
 */
public class GeoArea implements Serializable {

    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String name;
    @SerializedName("street")
    private String street;
    @SerializedName("city")
    private String city;
    @SerializedName("state")
    private String state;
    @SerializedName("zip")
    private Object zip;
    @SerializedName("country")
    private String country;
    @SerializedName("lattitude")
    private Double latitude;
    @SerializedName("longitude")
    private Double longitude;
    @SerializedName("radius")
    private Float radius;
    @SerializedName("createdAt")
    private Date createdAt;
    @SerializedName("updatedAt")
    private Date updatedAt;

    @SerializedName("beacons")
    private List<BeaconParams> beacons;
    @SerializedName("activeTimes")
    private List<ActiveTime> activeTimes;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getStreet() {
        return street;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public Object getZip() {
        return zip;
    }

    public String getCountry() {
        return country;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Float getRadius() {
        return radius;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public List<BeaconParams> getBeacons() {
        return beacons;
    }

    public List<ActiveTime> getActiveTimes() {
        return activeTimes;
    }
}
