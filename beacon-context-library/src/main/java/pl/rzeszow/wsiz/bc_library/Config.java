package pl.rzeszow.wsiz.bc_library;

/**
 * Created by Roman Savka on 02.03.2016.
 */
public final class Config {
    private Config() {
    }

    public static final int API_TIMEOUT_IN_SEC = 10;
    public static final long LOCATION_SERVICE_TIME_OUT_MS = 100;

    public static final String KONTAKT_IO_API_KEY = "xLIiqHSYQmUeMUmNtOQuGMWzLMmYMqtX";

    public static final String API_URL_DEV =
            "http://192.168.1.112/BeaconContext/api/";

    public static final String API_URL_PROD =
            "http://beacon-context.azurewebsites.net/api/";
}
