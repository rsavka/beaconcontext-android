package pl.rzeszow.wsiz.bc_library.api.interceptor;

import android.content.Context;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

import pl.rzeszow.wsiz.bc_library.utils.DeviceIdFactory;
import pl.rzeszow.wsiz.bc_library.utils.Logger;

/**
 * Created by Roman Savka on 02.03.2016.
 */
public class HeadersInterceptor implements Interceptor {

    private final String authInfo;
    private final String deviceInfo;

    public HeadersInterceptor(Context context, String appId, String apiKey) {
        this.authInfo = String.format("amx %s:%s", appId, apiKey);
        this.deviceInfo = String.format("android:%s", DeviceIdFactory.getUuid(context));
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();
        Request.Builder builder = original.newBuilder()
                .header("Authorization", authInfo)
                .header("Device-Info", deviceInfo)
                .method(original.method(), original.body());
        Logger.d("Request:" + original.urlString());
        return chain.proceed(builder.build());
    }
}
