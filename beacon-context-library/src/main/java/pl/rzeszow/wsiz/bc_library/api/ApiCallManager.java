package pl.rzeszow.wsiz.bc_library.api;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import pl.rzeszow.wsiz.bc_library.Config;
import pl.rzeszow.wsiz.bc_library.api.interceptor.HeadersInterceptor;
import pl.rzeszow.wsiz.bc_library.helper.Configuration;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by Roman Savka on 04.03.2016.
 */
public final class ApiCallManager {

    private static final String TAG = "ApiCallManager";

    private static ApiCallManager instance = new ApiCallManager();

    private ApiServiceEndpoint apiServiceEndpoint;

    private ApiCallManager() {
    }

    public static ApiCallManager getInstance() {
        return instance;
    }

    public static ApiServiceEndpoint getEndpoint() {
        return instance.apiServiceEndpoint;
    }

    public static void initServiceEndpoint(Context context) {
        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(Config.API_TIMEOUT_IN_SEC, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(Config.API_TIMEOUT_IN_SEC, TimeUnit.SECONDS);
        okHttpClient.interceptors()
                .add(new HeadersInterceptor(context, Configuration.getAppId(), Configuration.getApiKey()));

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Config.API_URL_DEV)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        instance.apiServiceEndpoint = retrofit.create(ApiServiceEndpoint.class);
    }
}
