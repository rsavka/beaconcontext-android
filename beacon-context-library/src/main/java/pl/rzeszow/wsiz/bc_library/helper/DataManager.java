package pl.rzeszow.wsiz.bc_library.helper;

import android.content.Context;

import com.google.android.gms.location.Geofence;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import pl.rzeszow.wsiz.bc_library.api.ApiCallManager;
import pl.rzeszow.wsiz.bc_library.model.ActiveTime;
import pl.rzeszow.wsiz.bc_library.model.BeaconParams;
import pl.rzeszow.wsiz.bc_library.model.GeoArea;
import pl.rzeszow.wsiz.bc_library.utils.GeofenceFactory;
import pl.rzeszow.wsiz.bc_library.utils.Logger;
import pl.rzeszow.wsiz.bc_library.utils.StorageManager;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Roman Savka on 05.03.2016.
 */
public final class DataManager implements Serializable {

    private static final String TAG = "DataManager";
    private static final String DATA_FILE_NAME = "beacon.context.data";
    private static final long DATA_MAX_SYNC_THRESHOLD = TimeUnit.HOURS.toMillis(1);

    private static DataManager instance;
    private Context context;
    private Data data;

    private DataManager() {
    }

    public static synchronized void initialize(Context context) {
        getInstance().context = context;
        getInstance().loadDataFromFile();
    }

    private void loadDataFromFile() {
        data = StorageManager.readFromFile(context, DATA_FILE_NAME);
    }

    public void saveData() {
        StorageManager.saveToFile(context, DATA_FILE_NAME, data);
    }

    public static DataManager getInstance() {
        if (instance == null) {
            instance = new DataManager();
        }
        return instance;
    }

    public boolean isDataLoaded() {
        return data != null && !data.isEmpty();
    }

    public boolean isDataUpToDate() {
        return isDataLoaded() &&
                data.syncTimestamp > 0 &&
                data.syncTimestamp + DATA_MAX_SYNC_THRESHOLD > System.currentTimeMillis();
    }

    public void clear() {
        if (isDataLoaded()) {
            List<String> geofences = new ArrayList<>();
            for (GeoArea area : data.geoAreasList) {
                ActiveTimeHelper.remove(context, area.getActiveTimes());
                geofences.add(area.getId());
            }
            GeofenceHelper.getInstance(context).remove(geofences);
            data.geoAreasList.clear();
            data.geoAreasMap.clear();
            data = null;
        }
    }

    public void load() {
        if (!isDataLoaded() || !isDataUpToDate()) {
            ApiCallManager.getEndpoint().getAreas().enqueue(new Callback<List<GeoArea>>() {
                @Override
                public void onResponse(Response<List<GeoArea>> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        clear();
                        processData(response.body());
                        saveData();
                    } else {
                        Logger.d("Sync endpoint failed");
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    Logger.d(t.getMessage());
                }
            });
        } else if (GeofenceHelper.wasInternalError(context)) {
            GeofenceHelper.removeInternalErrorFlag(context);
            monitorAreas();
        }
    }

    private void processData(List<GeoArea> data) {
        this.data = new Data(data);
        scheduleActiveTimes();
        monitorAreas();
    }

    private void scheduleActiveTimes() {
        for (GeoArea area : data.geoAreasList) {
            List<ActiveTime> times = area.getActiveTimes();
            if (times != null && !times.isEmpty()) {
                ActiveTimeHelper.schedule(context, times);
            }
        }
    }

    private void monitorAreas() {
        List<Geofence> geofences = new ArrayList<>();
        for (GeoArea area : data.geoAreasList) {
            List<ActiveTime> times = area.getActiveTimes();
            if (times == null || times.isEmpty()) {
                geofences.add(GeofenceFactory.createGeofence(area));
            }
        }
        if (!geofences.isEmpty()) {
            GeofenceHelper.getInstance(context).add(geofences);
        }
    }

    public GeoArea getArea(String id) {
        return data.geoAreasMap.get(id);
    }

    public BeaconParams matchParams(BeaconParams params) {
        for (GeoArea a : data.geoAreasList) {
            for (BeaconParams p : a.getBeacons()) {
                if (p.equals(params)) {
                    return p;
                }
            }
        }
        return null;
    }

    private static class Data implements Serializable {

        private List<GeoArea> geoAreasList;
        private HashMap<String, GeoArea> geoAreasMap;
        private long syncTimestamp;

        public Data(List<GeoArea> list) {
            syncTimestamp = System.currentTimeMillis();
            geoAreasList = list;
            geoAreasMap = new HashMap<>();
            for (GeoArea a : geoAreasList) {
                geoAreasMap.put(a.getId(), a);
            }
        }

        public boolean isEmpty() {
            return geoAreasList == null || geoAreasList.isEmpty();
        }
    }
}

