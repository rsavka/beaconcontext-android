package pl.rzeszow.wsiz.bc_library.reciever;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import java.util.List;

import pl.rzeszow.wsiz.bc_library.model.ActionAttr;
import pl.rzeszow.wsiz.bc_library.model.BeaconAction;
import pl.rzeszow.wsiz.bc_library.ui.WebViewActivity;
import pl.rzeszow.wsiz.bc_library.utils.Logger;

/**
 * Created by Roman Savka on 10.03.2016.
 */
public class ActionReceiver extends BroadcastReceiver {

    public static final String RECEIVE_ACTION = "pl.rzeszow.wsiz.bc_library.reciever.RECEIVE_ACTION";
    public static final String SEND_ACTION_NOTIFICATION = "pl.rzeszow.wsiz.bc_library.reciever.SEND_ACTION_NOTIFICATION";
    public static final String TAP_ACTION_NOTIFICATION = "pl.rzeszow.wsiz.bc_library.reciever.TAP_ACTION_NOTIFICATION";
    public static final String DISMISS_ACTION_NOTIFICATION = "pl.rzeszow.wsiz.bc_library.reciever.DISMISS_ACTION_NOTIFICATION";

    public static final String ACTION = "ACTION";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (TAP_ACTION_NOTIFICATION.equals(intent.getAction())) {
            Logger.d("action notification opened");
            performAction(context, (BeaconAction) intent.getSerializableExtra(ACTION));
        } else if (DISMISS_ACTION_NOTIFICATION.equals(intent.getAction())) {
            Logger.d("action notification dismissed");
        } else if (SEND_ACTION_NOTIFICATION.equals(intent.getAction())) {
            Logger.d("action notification was sent");
        } else if (RECEIVE_ACTION.equals(intent.getAction())) {
            Logger.d("receive action without notification params");
        }
    }

    private void performAction(Context context, BeaconAction action) {
        List<ActionAttr> attributes = action.getAttributes();
        for (ActionAttr attr : attributes) {
            if (ActionAttr.HTML_KEY.equals(attr.getKey())) {
                Intent intent = new Intent(context, WebViewActivity.class);
                intent.putExtra(WebViewActivity.ACTION, action);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            } else if (ActionAttr.LINK_KEY.equals(attr.getKey())) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(attr.getValue()));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        }
    }
}
